all: install

install: update submodule

force: reset update submodule

reset:
	git reset --hard

update:
	git pull origin master

submodule:
	git submodule update --init --recursive
	git submodule foreach git pull origin master

nvim:
	[ -e ~/.config/nvim ] ln -s ~/.vim ~/.config/nvim
	ln -fs ~/.config/nvim/vimrc ~/.config/nvim/init.vim || True

clean:
	rm -rf bundle vimrc colors
